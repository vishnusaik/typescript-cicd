import express from 'express'
import { greeting } from './user'

const app = express()
app.get('/', (req,res) => res.send('This App is running properly!'))
app.get('/ping', (req,res) => res.send('sucess 200!'))
app.get('/home', (req,res) => res.send('Ini Homepage!'))
app.get('/yunan', (req,res) => res.send('project cicd by yunan!'))
app.get('/hello/:name', (req,res) => {
    res.json({message:greeting(req.params.name)})
})

export default app